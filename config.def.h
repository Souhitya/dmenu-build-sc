/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int colorprompt = 1;                /* -p  option; if 1, prompt uses SchemeSel, otherwise SchemeNorm */
static int fuzzy = 1;                      /* -F  option; if 0, dmenu doesn't use fuzzy matching     */
static int centered = 0;                    /* -c option; centers dmenu on screen */
static int min_width = 500;                    /* minimum width when centered */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
//	"Fira Code:size=10"
	"Iosevka SS04:size=10"
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	/* [SchemeNorm] = { "#bbbbbb", "#222222" }, */
	/* [SchemeSel] = { "#eeeeee", "#005577" }, */
    /* [SchemeSelHighlight] = { "#ffc978", "#005577" }, */
    /* [SchemeNormHighlight] = { "#ffc978", "#222222" }, */
	/* [SchemeOut] = { "#000000", "#00ffff" }, */
	/* [SchemeMid] = { "#eeeeee", "#770000" }, */
	[SchemeNorm] = { "#bbbbbb", "#0a1b21" },
	[SchemeSel] = { "#0a1b21", "#f9c74f" },
    [SchemeSelHighlight] = { "#d62828", "#f9c74f" },
    [SchemeNormHighlight] = { "#f94144", "#2d6a4f" },
	[SchemeOut] = { "#2d6a4f", "#00ffff" },
	[SchemeMid] = { "#eeeeee", "#2d6a4f" },
};
/* -l and -g options; controls number of lines and columns in grid if > 0 */
static unsigned int lines      = 4;
static unsigned int columns    = 6;
/* -h option; minimum height of a menu line */
static unsigned int lineheight = 0;
static unsigned int min_lineheight = 8;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static unsigned int border_width = 2;
